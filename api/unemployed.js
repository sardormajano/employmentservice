import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

export const Unemployed = new Mongo.Collection('unemployed');

if(Meteor.isServer)
{
  Meteor.publish('unemployed', function unemployedPublication(){
    return Unemployed.find();
  });
  
  Meteor.methods({
    'unemployed.insert'(data)
    {
      Unemployed.insert(data);
    }
  })
}
