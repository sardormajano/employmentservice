FlowRouter.route('/', {
  action()
  {
    BlazeLayout.render('mainLayout', {
      content: 'employmentService'
    });
  }
});

FlowRouter.route('/forUnemployed', {
  action(){
    BlazeLayout.render('mainLayout', {
      content: 'forUnemployed'
    });
  }
});

FlowRouter.route('/forEmployees', {
  action(){
    BlazeLayout.render('mainLayout', {
      content: 'forEmployees'
    });
  }
});

FlowRouter.route('/createUnemployed', {
  action(){
    BlazeLayout.render('mainLayout', {
      content: 'createUnemployed'
    });
  }
});
