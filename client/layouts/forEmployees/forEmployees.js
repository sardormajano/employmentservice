import { Template } from 'meteor/templating';
import './forEmployees.html';
import { Unemployed } from '../../../api/unemployed.js';

Meteor.subscribe('unemployed');

Template.forEmployees.onRendered(function forEmployeesOnRendered(){
  $('select').material_select();
});

Template.forEmployees.helpers({
  unemployed()
  {
    return Unemployed.find();
  }
});
