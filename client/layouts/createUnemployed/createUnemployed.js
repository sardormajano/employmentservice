import { Template } from 'meteor/templating';

import './createUnemployed.html';

Template.createUnemployed.onRendered(function createUnemployedOnRendered(){
  $('#create-unemployed-form .datepicker').pickadate({
    selectMonths: true,
    selectYears: 100
  });

  $('select').material_select();
});

Template.createUnemployed.events({
  'click #create-unemployed-button'(e, t)
  {
    e.preventDefault();

    const firstName = $("#cu-first-name").val(),
          lastName = $("#cu-last-name").val(),
          email = $("#cu-email").val(),
          birthdate = $("#cu-birthdate").val(),
          maritalStatus = $("#cu-marital-status").val(),
          sex = $("#cu-sex").val(),
          position = $("#cu-position").val(),
          photo = $("#cu-sex").val() === "муж" ? "http://image005.flaticon.com/1/svg/145/145867.svg" : "http://image005.flaticon.com/1/svg/145/145852.svg"
          data = { firstName, lastName, email, birthdate, maritalStatus, photo };

    Meteor.call('unemployed.insert', data);
    FlowRouter.go('/forEmployees');
  }
});
